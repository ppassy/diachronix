<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fr_FR">
<context>
    <name>ActiveBandsEvolution_classDialogBase</name>
    <message>
        <location filename="../active_channel_evolution_dialog_base.ui" line="43"/>
        <source>Process to be performed</source>
        <translation>Tâches à effectuer</translation>
    </message>
    <message>
        <location filename="../active_channel_evolution_dialog_base.ui" line="561"/>
        <source>Valley bottom blocks</source>
        <translation>Blocs de fond de vallée</translation>
    </message>
    <message>
        <location filename="../active_channel_evolution_dialog_base.ui" line="95"/>
        <source>Active channel blocks</source>
        <translation>Blocs de bande active</translation>
    </message>
    <message>
        <location filename="../active_channel_evolution_dialog_base.ui" line="121"/>
        <source>Valley bottom and active channel blocks</source>
        <translation>Blocs de fond de vallée et de bande active</translation>
    </message>
    <message>
        <location filename="../active_channel_evolution_dialog_base.ui" line="172"/>
        <source>Valley bottom</source>
        <translation>Fond de vallée</translation>
    </message>
    <message>
        <location filename="../active_channel_evolution_dialog_base.ui" line="214"/>
        <source>To detect the starting point</source>
        <translation>Détecter le point initial</translation>
    </message>
    <message>
        <location filename="../active_channel_evolution_dialog_base.ui" line="234"/>
        <source>Distance between two cross-sections</source>
        <translation>Distance entre deux transects</translation>
    </message>
    <message>
        <location filename="../active_channel_evolution_dialog_base.ui" line="330"/>
        <source>metres</source>
        <translation>mètres</translation>
    </message>
    <message>
        <location filename="../active_channel_evolution_dialog_base.ui" line="298"/>
        <source>Cross-sections widths</source>
        <translation>Largeur des transects</translation>
    </message>
    <message>
        <location filename="../active_channel_evolution_dialog_base.ui" line="347"/>
        <source>Valley bottom blocks (output)</source>
        <translation>Blocs de fond de vallée (sortie)</translation>
    </message>
    <message>
        <location filename="../active_channel_evolution_dialog_base.ui" line="670"/>
        <source>...</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../active_channel_evolution_dialog_base.ui" line="396"/>
        <source>Table of valley bottom blocks (output)</source>
        <translation>Tableau des blocs de fond de vallée (sortie)</translation>
    </message>
    <message>
        <location filename="../active_channel_evolution_dialog_base.ui" line="436"/>
        <source>To reverse the starting point</source>
        <translation>Inverser le point inital</translation>
    </message>
    <message>
        <location filename="../active_channel_evolution_dialog_base.ui" line="687"/>
        <source>To load the temporary layers</source>
        <translation>Charger les couches temporaires</translation>
    </message>
    <message>
        <location filename="../active_channel_evolution_dialog_base.ui" line="519"/>
        <source>Active channel</source>
        <translation>Bande active</translation>
    </message>
    <message>
        <location filename="../active_channel_evolution_dialog_base.ui" line="598"/>
        <source>Active channel blocks (output)</source>
        <translation>Blocs de bande active (sortie)</translation>
    </message>
    <message>
        <location filename="../active_channel_evolution_dialog_base.ui" line="647"/>
        <source>Table of active channel blocks (output)</source>
        <translation>Tableau des blocs de bandes actives (sortie)</translation>
    </message>
    <message>
        <location filename="../active_channel_evolution_dialog_base.ui" line="20"/>
        <source>Diachronix</source>
        <translation>Diachronix</translation>
    </message>
</context>
<context>
    <name>active_channel_evolution</name>
    <message>
        <location filename="../active_channel_evolution.py" line="194"/>
        <source>&amp;Diachronix</source>
        <translation>&amp;Diachronix</translation>
    </message>
    <message>
        <location filename="../active_channel_evolution.py" line="181"/>
        <source>Building of the blocks</source>
        <translation>Construction des blocs</translation>
    </message>
    <message>
        <location filename="../active_channel_evolution.py" line="200"/>
        <source>Select output layer for valley bottom blocks</source>
        <translation>Sélectionner la couche de sortie des blocs de fond de vallée</translation>
    </message>
    <message>
        <location filename="../active_channel_evolution.py" line="211"/>
        <source>GeoPackage files (*.gpkg);;ESRI Shapefile (*.shp)</source>
        <translation>GeoPackage (*.gpkg);;ESRI Shapefile (*.shp)</translation>
    </message>
    <message>
        <location filename="../active_channel_evolution.py" line="205"/>
        <source>Select output table of the valley bottom blocks</source>
        <translation>Sélectionner le tableau des blocs de fond de vallée</translation>
    </message>
    <message>
        <location filename="../active_channel_evolution.py" line="221"/>
        <source>Comma Separated Values [CSV] (*.csv);;Microsoft Excel files (*.xlsx);;Open Document Spreadsheet (*.ods)</source>
        <translation>Fichier séparé par des virgules [CSV] (*.csv);;Fichier Microsoft Excel (*.xlsx);;Fichier Open Document Spreadsheet (*.ods)</translation>
    </message>
    <message>
        <location filename="../active_channel_evolution.py" line="211"/>
        <source>Select the layer of the valley bottom blocks to use</source>
        <translation>Sélectionner la couche des blocs de fond de vallée à utiliser</translation>
    </message>
    <message>
        <location filename="../active_channel_evolution.py" line="216"/>
        <source>Select output file for the active channel blocks</source>
        <translation>Sélectionner la couche des blocs de bande active</translation>
    </message>
    <message>
        <location filename="../active_channel_evolution.py" line="216"/>
        <source>GeoPackage files (*.gpkg);;ESRI Shapefile  (*.shp)</source>
        <translation>GeoPackage (*.gpkg);;ESRI Shapefile (*.shp)</translation>
    </message>
    <message>
        <location filename="../active_channel_evolution.py" line="221"/>
        <source>Select output table of the active channel blocks</source>
        <translation>Sélectionner le tableau des blocs de bande activre</translation>
    </message>
    <message>
        <location filename="../active_channel_evolution.py" line="274"/>
        <source>Valley bottom blocks</source>
        <translation>Blocs de fond de vallée</translation>
    </message>
    <message>
        <location filename="../active_channel_evolution.py" line="275"/>
        <source>Valley bottom centerline</source>
        <translation>Ligne centrale de fond de vallée</translation>
    </message>
    <message>
        <location filename="../active_channel_evolution.py" line="276"/>
        <source>Valley bottom cross-sections</source>
        <translation>Transects de fond de vallée</translation>
    </message>
    <message>
        <location filename="../active_channel_evolution.py" line="277"/>
        <source>Valley bottom river kilometers</source>
        <translation>PK de fond de vallée</translation>
    </message>
    <message>
        <location filename="../active_channel_evolution.py" line="278"/>
        <source>Active channel blocks</source>
        <translation>Blocs de bande active</translation>
    </message>
    <message>
        <location filename="../active_channel_evolution.py" line="279"/>
        <source>Active channel centerline</source>
        <translation>Ligne centrale de bande active</translation>
    </message>
    <message>
        <location filename="../active_channel_evolution.py" line="280"/>
        <source>Starting point</source>
        <translation>Point initial</translation>
    </message>
    <message>
        <location filename="../active_channel_evolution.py" line="331"/>
        <source>Step 1 - Valley bottom processing</source>
        <translation>Étape 1 : Traitement du fond de vallée</translation>
    </message>
    <message>
        <location filename="../active_channel_evolution.py" line="339"/>
        <source>Step 2 - Active channel processing</source>
        <translation>Étape 2 : Traitement de la bande active</translation>
    </message>
    <message>
        <location filename="../active_channel_evolution.py" line="365"/>
        <source>Something went wrong. See Python console for details :/</source>
        <translation>Mauvaise nouvelle... une erreur est survenue. Voir la console Python pour plus de détails :/</translation>
    </message>
</context>
</TS>
