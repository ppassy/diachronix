<p><a href="https://ofb.gouv.fr/"><img alt="Logo OFB" src="https://ofb.gouv.fr/sites/default/files/logo-ofb.png" height="50"></a> <a href="https://u-paris.fr/"><img alt="Logo UPC" src="https://u-paris.fr/wp-content/uploads/2022/03/UniversiteParisCite_logo_horizontal_couleur_RVB.jpg" height="80"></a> <a href="https://www.inrae.fr/"><img alt="Logo INRAE" src="https://seeklogo.com/images/I/inrae-logo-3BC2534099-seeklogo.com.png" height="40"></a> <a href="https://univ-cotedazur.fr/"><img alt="Logo Université Côte d'Azur" src="http://www.imbrsea.eu/sites/default/files/portfolio/UCAlogoQlarge.png" height="50"></a> <a href="https://www.ensg.eu/"><img alt="Logo ENSG" src="https://cours-fad.ensg.eu/login_ensg/img/logos/logo-ensg.png" height="50"></a> <a href="https://www.cnrs.fr/"><img alt="Logo CNRS" src="https://www.cnrs.fr/themes/custom/cnrs/logo.svg" height="70"></a></p>

<h1>Diachronix plugin</h1>

<h2>Purpose of the plugin</h2>

<p>This plugin proposes an automatic method to segment both polygon layers of valley bottom and of active channels into blocks of equal lengths. Because the blocks of the valley bottom are used as a reference, all the active channel blocks are consistent though time. For each of them the mean width is calculated. Thanks to this method, it is possible to track over time the evolution of the widths of specific sections of an active channel (figure 1).</p>

<div align="center">
<figure>
<img src="./figures/aims.svg" alt="Aims" width="50%">
<figcaption><b>Figure 1: General purpose of the plugin.</b></figcaption>
</figure>
</div>

<h2>Necessary data</h2>

<p>This plugin can segment valley bottom layers or active channels layers. The geometry of these layers has to be of type <em>polygons</em>. The coordinate reference system of these layers have to use the <em>meters</em> as units. Both the valley bottom layer and the active channel layers have to be associated to only one river without any tributaries (figure 2).</p>

<div align="center">
<figure>
<img src="./figures/layers.svg" alt="Layers" width="50%">
<figcaption><b>Figure 2: Geometry of the input layers.</b></figcaption>
</figure>
</div>

<p>As shown on figure 2, if there are a main river and a tributary, the user has to create a valley bottom layer, or active channels layers, specifically for each river. The valley bottom layer could be manually digitized based on geological maps. The active channels layers could be manually digitized based on aerial photographs. It is better to start the digitizing by the upstream part of the valley bottom or of the active channel in order to follow the natural way of the river.</p>

<h2>General method</h2>

<p>The method on which is based this plugin was developed by Frédéric Liebault and Guillaume Piton (INRAE/ETNA). The user should provide a reference layer, which is in most of the cases the layer of the valley bottom. This valley bottom is the geological maximum extent of the flood plain. We do the hypothesis that the actual flood plain is entirely included within the extent of the valley bottom. As said previously, this layer may be digitized based on geological maps. However, if it is not possible to create this layer, the user may just create a very large buffer around the river. The buffer should be large enough to include totally the actual flood plain. Once this reference layer is set, the user should digitized one, or many, active channel layers. This digitizing may be based on aerial photographs. In the case the user would like to compare active channels at different dates, one layer per date should be digitized.</p>

<p>Then, the reference layer is segmented in blocks of equal lengths. The length is provided by the user. An <em>id</em> and a distance from the starting point are associated to each block of the reference layer. After this segmentation, the active channel layers are clipped according to the blocks of the reference layer. The mean width is then calculated for each block of active channel. Thanks to this, the block number <em>n</em> of the reference layer always corresponds to the block number <em>n</em> of the active channel layers. So the user may track over time the evolution of the width of a specific block of active channel. The figure 1 gives an overview of the method.</p>

<h2>The use of the plugin</h2>

<h3>Prerequisites</h3>

<p>The <em>Diachronix</em> plugin requires at least QGIS 3.14. The plugin was successfully run on Windows and on Linux. In order to be run, the <em>Diachronix</em> plugin necessitates another plugin called <em>Geometric attributes</em>. Here, this plugin is used to calculate the centerlines of the different layers. However, the co-installation of this plugin should be automatic. Before to run the plugin the layers to be segmented should be loaded in QGIS.</p>

<h3>Running the plugin</h3>

<p>The plugin could be run either through the <em>Extensions</em> menu or by clicking on the corresponding icon. The main window of the plugin could be divided in three parts (figure 3).</p>

<div align="center">
<figure>
<img src="./figures/window_parts.svg" alt="Window" width="70%">
<figcaption><b>Figure 3: The main window of the plugin.</b></figcaption>
</figure>
</div>

<p>In the part 1, the user may choose :
- to calculate the blocks of the bottom valley layer (<em>i.e.</em> the reference layer) only, by checking the <em>Valley bottom blocks</em> box
- to calculate the blocks of the active channel layer only, by checking the <em>Active channel blocks</em> box
- to calculate the blocks of both the reference layer and of the active channel layer once, by checking the <em>Valley bottom and active channel blocks</em> box</p>

<h3>Segmentation of the reference layer</h3>

<p>First, it is recommended to run only the segmentation of the reference layer by checking the box <em>Valley bottom blocks</em>. Once this checkbox is checked, the part 2 of the main window becomes accessible. First, in the menu <em>Valley bottom</em>, the user should select the layer corresponding to the valley bottom, or to the reference layer to be used. At this step, if we are not sure if the polygon starts from the upstream part of the valley we can click on <em>To detect the starting point</em> and <em>OK</em>. A new point layer will appear showing the starting point of the layer. However, we may note it is not always obvious to determine the starting point.</p>

<p>Then the user may choose a length for the blocks to be calculated at the line <em>Distance between two cross-sections</em>. By default this length is set to 100 m. The user is free to use any length but very short lengths may produce some issues later on the process. The user should then set the <em>Cross-sections widths</em> to be used. This cross-section width should be large enough to always be larger than the maximum width of the layer to be segmented. </p>

<p>At the line <em>Valley bottom blocks (output)</em> the user sets a path and a name for the layer containing the resulting blocks which will be created. The format of this layer may be <em>shapefile</em> or <em>GeoPackage</em>. At the line <em>Table of valley bottom blocks (output)</em> the user sets a path and a name for the table which will contain the data relative to the resulting blocks. This table can be stored in <em>csv</em>, in Excel format (<em>.xlsx</em>) or in OpenDocument format (<em>.ods</em>). This table is exactly the same as the attribute table of the produced layer. The user may choose <em>To reverse the starting point</em> in order to start the blocks from the upstream part of the valley or from the downstream part of the valley. In order to check the intermediate layers, the user may load the temporary layers by clicking on <em>To load the temporary layers</em>. By clicking on <em>OK</em>, the blocks of the bottom valley layer will be produced. The process may take from some seconds to a few minutes according to the size of the layer to be segmented. The attribute table of the bottom valley blocks layer is shown on the figure 4.</p>

<div align="center">
<figure>
<img src="./figures/attribute_table.png" alt="Attribute table" width="60%">
<figcaption><b>Figure 4: The attribute table of the bottom valley blocks layer.</b></figcaption>
</figure>
</div>

<p>In this attribute table, the <em>fid</em> and <em>ID</em> fields are just two ID fields. Each block has a unique ID. The other fields are:
- <em>distance</em>: the distance in meters between the centroid of the block and the upstream part (or the downstream part) of the valley (figure 5).
- <em>area_m2</em>: the area in m<sup>2</sup> of each block
- <em>length_m</em>: the length in meter of each block along its centerline. These lengths are always very close to the length set by the user for the <em>Distance between two cross-sections</em>.
- <em>mean_width_m</em>: the mean width of each block in meter. This mean width is simply the ratio of the area of each block by the length of each block.</p>

<div align="center">
<figure>
<img src="./figures/distance_blocks.svg" alt="Distance between blocks" width="50%">
<figcaption><b>Figure 5: The distance of each block.</b></figcaption>
</figure>
</div>

<p>The plugin also exports the result in a simple table format (<em>.csv</em>, <em>.xlsx</em> or <em>.ods</em>). This table is exactly the same as the attribute table of the exported layer but allows the user to directly work on it outside from QGIS (figure 6).</p>

<div align="center">
<figure>
<img src="./figures/table_ods.png" alt="Simple table" width="60%">
<figcaption><b>Figure 6: The exported table opened in LibreOffice Calc.</b></figcaption>
</figure>
</div>

<h3>Segmentation of the active channel layer</h3>

<p>Once the reference layer is segmented, we can use it to segment the active channel layer. To do it, in the part of the main window called <em>Process to be performed</em>, the user has to check <em>Active channel blocks</em>. By doing this, the <em>Active channel</em> part of the window becomes available. At the line <em>Active channel</em>, the user should select the layer of the active channel to be segmented. At the line <em>Valley bottom blocks</em>, the user should set the path to the reference layer to be used for the segmentation. This reference layer is the one produced at the previous step. These two layers may be in <em>shapefile</em> or in <em>GeoPackage</em>. At the line <em>Active channel blocks (output)</em>, the user should set the path and the name of the layer which will be produced after the segmentation of the active channel. At the line <em>Table of active channel blocks (output)</em>, the user should set the path and the name of the resulting simple table. This table could be in <em>.csv</em>, <em>.xlsx</em> or in <em>.ods</em>. Finally, if wished it is possible to load the temporary layers by checking the line <em>To load the temporary layers</em>.</p>

<p>As a result, we obtain the layer of the segmented active channel. The fields of the attribute table are exactly the same as the ones of the <em>Bottom valley blocks</em>. It is the same for the simple exported table.</p>

<h3>Segmentation of the reference layer and of the active channel layer in once</h3>

<p>The user may also run the segmentation of the reference layer and of the active channel layer in once. To do this, the user should check the <em>Valley bottom and active channel blocks</em> option in the part <em>Process to be performed</em> of the main window. Once this option is checked, both the parts dedicated to the segmentation of the reference layer and to the segmentation of the active channel layer becomes available. The only change takes part in the part dedicated to the segmentation of the active channel layer. The user should not any more set the path to the segmented reference layer to be used because the process will automatically use the one produced here.</p>

<h2>Limitations</h2>

<p>The plugin suffers from some limitations listed below.</p>

<h3>The distance between two cross-sections</h3>

<p>As said previously, the distance between two cross-sections is freely set by the user. However, this distance should not be too small in order to avoid some issues in the building of the blocks. The figure 7 shows the issues when this distance is too short.</p>

<div align="center">
<figure>
<img src="./figures/distance_too_short.png" alt="Distance too short" width="50%">
<figcaption><b>Figure 7: Effect of a distance too short (5 m in this example) on the resulting blocks.</b></figcaption>
</figure>
</div>

<p>To avoid this issue, we have to set a larger distance between two cross-sections.</p>

<h3>The width of the cross-sections</h3>

<p>The width of the cross-sections is freely set by the user. However, this width should be larger than the maximum width of the layer to be segmented. Otherwise, some blocks will be missing, resulting in blocks of non equal lengths (figure 8).</p>

<div align="center">
<figure>
<img src="./figures/cross-sections_too_small.svg" alt="Cross-sections too short" width="65%">
<figcaption><b>Figure 8: Effects of the cross-sections widths on the resulting blocks.</b></figcaption>
</figure>
</div>

<p>As shown on the figure 8, the blocks 1 and 2 are not well segmented. They are two times longer than the normal blocks. This is explained by the cross-sections 1 and 2 used for the segmentation. These two cross-sections are too short compared to the maximum width of the layer to be segmented. To avoid this issue, we have to set a larger value for the width of the cross-section.</p>

<h3>The cross-sections and the centerlines</h3>

<p>In the initial method, the cross-sections should be perpendicular to the centerlines of the layers to be segmented. However, this notion of <em>perpendicular</em> is not so obvious to implement in an automatic method. Thus, in this plugin the cross-sections are not always perpendicular to the cross-sections as shown on the figure 9.</p>

<div align="center">
<figure>
<img src="./figures/perpendicular.svg" alt="Cross-sections not perpendicular" width="40%">
<figcaption><b>Figure 9: The cross-section 1 is not perpendicular to the centerline.</b></figcaption>
</figure>
</div>

<p>As shown on the figure 9, the cross-section 1 is not perpendicular to the centerline. Because of this, the area of the block 1 is slightly underestimated compared to the block 2 which is slightly overestimated. However, this limitation has relatively few impacts on the calculation of the mean widths of the blocks.</p>

<h3>High sinuosity</h3>

<p>In the case of rivers with high sinuosity, some issues may appear in the meanders. In the meanders, one cross-section may cut two times the layer to be segmented. When this case happens, some blocks are missing (figure 10).</p>

<div align="center">
<figure>
<img src="./figures/sinuosity.svg" alt="Sinuosity" width="50%">
<figcaption><b>Figure 10: The cross-section 1 cuts two times the layer to be segmented, therefore the block *a* is missing.</b></figcaption>
</figure>
</div>

<p>To avoid this issue, the user may shorten the width of the cross-sections or make the distance between the cross-sections larger.</p>

<h2>Authors, contributors and contact</h2>

<p>The geomorphological part was carried out by Frédéric Liébault (INRAE), Guillaume Piton (INRAE), Margot Chapuis (Université Côte d'Azur) and Gabriel Melun (OFB). The implementation in Python was done by Paul Passy (Université Paris Cité - PRODIG), Sylvain Théry (CNRS - ART-Dev) and students in geomatics (ENSG) : Judith Nabec, Estelle Stefanini, Damien D-Arras and Fernando Rico-Quintero.</p>

<h3>Contact</h3>

<p>For any comments or questions, feel free to contact the developpers: paul.passy[at]u-paris.fr, sylvain.thery[at]cnrs.fr</p>
