# -*- coding: utf-8 -*-
# this script detects the first digitized point of the valley bottom

def detect_starting_point(QgsProject, outputs, QgsProcessing, processing, selected_layer_vb, name_starting_point):
    """
    TODO
    We extract the vertex 0 of the polygon layer of the valley bottom

    :param QgsProject: the instance of the QGIS project
    :type QgsProject: QGIS project
    :param outputs: a dictionary to store the outputs of the QGIS processes
    :type outputs: dictionary
    :param QgsProcessing: the QGIS processes
    :type QgsProcessing: QgsProcessing
    :param processing: the QGIS processes
    :type processing: processing
    :param selected_layer_vb: the polygon vector layer of the valley bottom
    :type selected_layer_vb: vector layer
    :param name_starting_point: the name which will be displayed for the resulting layer
    :type name_starting_point: string
    :return: loads the temporary layer of the starting point
    :rtype:
    """

    alg_params = {
        'INPUT': selected_layer_vb,
        'VERTICES':'0',
        'OUTPUT': QgsProcessing.TEMPORARY_OUTPUT
    }
    outputs['vb_start_point'] = processing.run('native:extractspecificvertices', alg_params)

    QgsProject.instance().addMapLayer(outputs['vb_start_point']['OUTPUT']).setName(name_starting_point)
